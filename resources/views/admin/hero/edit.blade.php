@extends('admin.layout.layout')
@section('content')
<section class="content">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Data</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" id="form-survey" method="post" action="/helcurt/hero/update/{{$cek->id}}/{{$cek->hero_name}}">
            @csrf
            <div class="box-body">
              <div class="form-group">
                <label for="hero_name" class="col-sm-2 control-label">Hero Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="hero_name" placeholder="Hero Name" name="hero_name" value="{{$cek->hero_name}}"
                    required>
                </div>
              </div>
              <div class="form-group">
                <label for="logo" class="col-sm-2 control-label">Logo</label>
                <div class="col-sm-10">
                  <input type="file" class="form-control" id="logo" placeholder="Nama Survey" name="logo">
                </div>
              </div>
              <div class="form-group">
                <label for="durability" class="col-sm-2 control-label">Durability</label>
                <div class="col-sm-10">
                  <input type="number" min="0" max="100" value="{{$cek->durability}}" class="form-control" id="durability" placeholder="0 - 100" name="durability"
                    required>
                </div>
              </div>
              <div class="form-group">
                <label for="offense" class="col-sm-2 control-label">Offense</label>
                <div class="col-sm-10">
                  <input type="number" min="0" max="100" value="{{$cek->offense}}" class="form-control" id="offense" placeholder="0 - 100" name="offense"
                    required>
                </div>
              </div>
              <div class="form-group">
                <label for="ability_effects" class="col-sm-2 control-label">Ability Effects</label>
                <div class="col-sm-10">
                  <input type="number" min="0" max="100" value="{{$cek->ability_effects}}" class="form-control" id="ability_effects" placeholder="0 - 100" name="ability_effects"
                    required>
                </div>
              </div>
              <div class="form-group">
                <label for="difficulty" class="col-sm-2 control-label">Difficulty</label>
                <div class="col-sm-10">
                  <input type="number" min="0" max="100" value="{{$cek->difficulty}}" class="form-control" id="difficulty" placeholder="0 - 100" name="difficulty"
                    required>
                </div>
              </div>
              <div class="form-group">
                <label for="role1" class="col-sm-2 control-label">Role 1</label>
                <div class="col-sm-10">
                  <select class="form-control" id="role1" name="role1" onchange="look()">
                    <option value="1" {{$cek->role1 == 1 ? 'selected':''}}>Mage</option>
                    <option value="2" {{$cek->role1 == 2 ? 'selected':''}}>Assasin</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="role2" class="col-sm-2 control-label">Role 2</label>
                <div class="col-sm-10">
                  <select class="form-control" id="role2" name="role2">
                    <option selected disabled>-- Select Role --</option>
                    <option value="1" {{$cek->role2 == 1 ? 'selected':''}}>Mage</option>
                    <option value="2" {{$cek->role2 == 2 ? 'selected':''}}>Assasin</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="bio" class="col-sm-2 control-label">Bio</label>
                <div class="col-sm-10">
                  <textarea class="form-control" id="bio" placeholder="Bio" name="bio">{{$cek->bio}}</textarea>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <a href="{{route('admin-hero-index')}}" class="btn btn-dark">Cancel</a>
              <button type="submit" name="submit" id="action" class="btn btn-info pull-right">Update</button>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
      </div>
      <!-- /.modal-content -->
</section>
@endsection