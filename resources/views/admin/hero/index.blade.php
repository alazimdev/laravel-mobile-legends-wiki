@extends('admin.layout.layout')
@section('head')
<link rel="stylesheet" href="{{asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection
@section('content')
<section class="content">
  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Add Data</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" id="form-survey" method="POST" action="{{route('admin-hero-create')}}" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
              <div class="form-group">
                <label for="hero_name" class="col-sm-2 control-label">Hero Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="hero_name" placeholder="Hero Name" name="hero_name"
                    required>
                </div>
              </div>
              <div class="form-group">
                <label for="logo" class="col-sm-2 control-label">Logo</label>
                <div class="col-sm-10">
                  <input type="file" class="form-control" id="logo" placeholder="Nama Survey" name="logo"
                    required>
                </div>
              </div>
              <div class="form-group">
                <label for="durability" class="col-sm-2 control-label">Durability</label>
                <div class="col-sm-10">
                  <input type="number" min="0" max="100" value="50" class="form-control" id="durability" placeholder="0 - 100" name="durability"
                    required>
                </div>
              </div>
              <div class="form-group">
                <label for="offense" class="col-sm-2 control-label">Offense</label>
                <div class="col-sm-10">
                  <input type="number" min="0" max="100" value="50" class="form-control" id="offense" placeholder="0 - 100" name="offense"
                    required>
                </div>
              </div>
              <div class="form-group">
                <label for="ability_effects" class="col-sm-2 control-label">Ability Effects</label>
                <div class="col-sm-10">
                  <input type="number" min="0" max="100" value="50" class="form-control" id="ability_effects" placeholder="0 - 100" name="ability_effects"
                    required>
                </div>
              </div>
              <div class="form-group">
                <label for="difficulty" class="col-sm-2 control-label">Difficulty</label>
                <div class="col-sm-10">
                  <input type="number" min="0" max="100" value="50" class="form-control" id="difficulty" placeholder="0 - 100" name="difficulty"
                    required>
                </div>
              </div>
              <div class="form-group">
                <label for="role1" class="col-sm-2 control-label">Role 1</label>
                <div class="col-sm-10">
                  <select class="form-control" id="role1" name="role1" onchange="look()">
                    <option selected disabled>-- Select Role --</option>
                    <option value="1" id="1">Mage</option>
                    <option value="2">Assasin</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="role2" class="col-sm-2 control-label">Role 2</label>
                <div class="col-sm-10">
                  <select class="form-control" id="role2" name="role2" disabled>
                    <option selected disabled>-- Select Role --</option>
                    <option value="1">Mage</option>
                    <option value="2">Assasin</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="bio" class="col-sm-2 control-label">Bio</label>
                <div class="col-sm-10">
                  <textarea class="form-control" id="bio" placeholder="Bio" name="bio"></textarea>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" name="submit" id="action" class="btn btn-info pull-right">Save</button>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>150</h3>

            <p>Hero Skill's</p>
          </div>
          <a href="#" class="small-box-footer">More <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <h3>{{$role}}
              <!-- <sup style="font-size: 20px">%</sup> -->
            </h3>

            <p>Hero Role{{$role > 1 ? "s" : ""}}</p>
          </div>
          <a href="{{route('admin-hero-role-index')}}" class="small-box-footer">More <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3>44</h3>

            <p>Hero Picture's</p>
          </div>
          <a href="#" class="small-box-footer">More <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Table Hero's</h3>
            <button type="button" name="bulk_delete" id="bulk_delete" class="btn btn-warning pull-right"><i class="fa fa-trash"></i> Delete Data Checked</button>
            <p class="pull-right"> </p>
            <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus"> </i> Add</button><br/>
            
          </div>
          <div class="box-body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped" id="example1">
                <thead>
                  <tr>
                    <th>Hero Name</th>
                    <th>Logo</th>
                    <th>Durability</th>
                    <th>Offense</th>
                    <th>Aility Effects</th>
                    <th>Difficulty</th>
                    <th>Role 1</th>
                    <th>Role 2</th>
                    <th>Bio</th>
                    <th>Action<input type="checkbox" id="master" class="pull-right"/></th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->

</section>
@section('script')
<script src="{{asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
    $('#example1').DataTable({
      'paging': true,
      'lengthChange': true,
      'searching': true,
      'ordering': true,
      'info': true,
      'autoWidth': true,
      'responsive': true,
      processing: true,
      serverSide: true,
      ajax: '{!! route('admin-hero-data') !!}',
      columns: [
        { data: 'hero_name', name: 'hero_name' },
        { data: 'logo', name: 'logo' },
        { data: 'durability', name: 'durability' },
        { data: 'offense', name: 'offense' },
        { data: 'ability_effects', name: 'ability_effects' },
        { data: 'difficulty', name: 'difficulty' },
        { data: 'role1', name: 'role1' },
        { data: 'role2', name: 'role2' },
        { data: 'new_bio'},
        { data: 'action', orderable: false, searchable: false, 'sWidth' : '100%', 'sTextAlign' : 'right'}
      ],
    });
    $(document).on('click', '#bulk_delete', function(){
        var id = [];
        if(confirm("Are you sure you want to Delete this data?"))
        {
            $('.user_checkbox:checked').each(function(){
                id.push($(this).val());
            });
            if(id.length > 0)
            {
                $.ajax({
                    url:"{{route('admin-hero-massdelete')}}",
                    method:"get",
                    data:{id:id},
                    success:function(data)
                    {
                        alert(data);
                        $('#example1').DataTable().ajax.reload();
                    }
                });
            }
            else
            {
                alert("Please select atleast one checkbox");
            }
        }
    });
  });
  function look(){
    var cek = document.getElementById('role2');
    cek.disabled = false;
  }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#master').on('click', function(e) {
         if($(this).is(':checked',true))  
         {
            $(".user_checkbox").prop('checked', true);  
         } else {  
            $(".user_checkbox").prop('checked',false);  
         }  
        });
    });
</script>
@endsection
@endsection