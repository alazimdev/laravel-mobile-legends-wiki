@extends('admin.layout.layout')
@section('content')
<section class="content">
  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Add Data</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" id="form-survey" method="post" action="{{route('admin-hero-role-create')}}" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
              <div class="form-group">
                <label for="role_name" class="col-sm-2 control-label">Role Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="role_name" placeholder="Role Name" name="role_name"
                    required>
                </div>
              </div>
              <div class="form-group">
                <label for="logo" class="col-sm-2 control-label">Logo</label>
                <div class="col-sm-10">
                  <input type="file" class="form-control" id="logo" placeholder="Nama Survey" name="logo"
                    required>
                </div>
              </div>
              <div class="form-group">
                <label for="desc" class="col-sm-2 control-label">Desc</label>
                <div class="col-sm-10">
                  <textarea class="form-control" id="desc" placeholder="Desc" name="desc"></textarea>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" name="submit" id="action" class="btn btn-info pull-right">Save</button>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <!-- ./col -->
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Table Hero Role's</h3>
            <button type="button" name="bulk_delete" id="bulk_delete" class="btn btn-warning pull-right"><i class="fa fa-trash"></i> Delete Data Checked</button>
            <p class="pull-right"> </p>
            <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus"> </i> Add</button><br/>
            
          </div>
          <div class="box-body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped" id="example1">
                <thead>
                  <tr>
                    <th>Role Name</th>
                    <th colspan="2">Logo</th>
                    <th>Description</th>
                    <th>Action<input type="checkbox" id="master" class="pull-right"/></th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($roles as $role)
                    <tr>
                        <td>{{$role->role_name}}</td>
                        <td>{{$role->logo}}</td>
                        <td><img src="/storage/image/role/100/{{$role->logo}}" alt="{{$role->logo}}"></td>
                        <td>{{str_limit($role->desc, 40, '...')}}</td>
                        <td>
                            <a href="role/edit/{{$role->id}}/{{$role->role_name}}" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i></a>
                            <a href="role/delete/{{$role->id}}/{{$role->role_name}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                            <input type="checkbox" name="user_checkbox[]" class="user_checkbox pull-right" value="{{$role->id}}" />
                        </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->

</section>
@endsection
@section('script')
<script>
  $(function () {
    $(document).on('click', '#bulk_delete', function(){
        var id = [];
        if(confirm("Are you sure you want to Delete this data?"))
        {
            $('.user_checkbox:checked').each(function(){
                id.push($(this).val());
            });
            if(id.length > 0)
            {
                $.ajax({
                    url:"{{route('admin-hero-role-massdelete')}}",
                    method:"get",
                    data:{id:id},
                    success:function(data)
                    {
                        location.reload();
                    }
                });
            }
            else
            {
                alert("Please select atleast one checkbox");
            }
        }
    });
  });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#master').on('click', function(e) {
         if($(this).is(':checked',true))  
         {
            $(".user_checkbox").prop('checked', true);  
         } else {  
            $(".user_checkbox").prop('checked',false);  
         }  
        });
    });
</script>
@endsection