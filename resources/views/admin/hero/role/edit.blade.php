@extends('admin.layout.layout')
@section('content')
<section class="content">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Data</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" id="form-survey" method="post" action="/helcurt/hero/role/update/{{$cek->id}}/{{$cek->hero_name}}">
            @csrf
            <div class="box-body">
              <div class="form-group">
                <label for="role_name" class="col-sm-2 control-label">Role Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="role_name" placeholder="Hero Name" name="role_name" value="{{$cek->role_name}}"
                    required>
                </div>
              </div>
              <div class="form-group">
                <label for="logo" class="col-sm-2 control-label">Logo</label>
                <div class="col-sm-5">
                  <input type="file" class="form-control" id="logo" placeholder="Nama Survey" name="logo">
                </div>
                <div class="col-sm-5">
                  <a href="{{asset('storage/image/role/300/'.$cek->logo)}}" class="form-control" target="_blank" style="display: block; height: 100%; width: 100%;">Lihat gambar disini...</a>
                </div>
              </div>
              <div class="form-group">
                <label for="desc" class="col-sm-2 control-label">Desc</label>
                <div class="col-sm-10">
                  <textarea class="form-control" id="desc" placeholder="Desc" name="desc">{{$cek->desc}}</textarea>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <a href="{{route('admin-hero-role-index')}}" class="btn btn-dark">Cancel</a>
              <button type="submit" name="submit" id="action" class="btn btn-info pull-right">Update</button>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
      </div>
      <!-- /.modal-content -->
</section>
@endsection