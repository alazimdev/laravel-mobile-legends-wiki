<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Wiki - Mobile Legends : Bang - Bang</title>

    <!-- Favicon -->
    <link rel="icon" href="{{asset('egames/img/ml-icon.png')}}">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{asset('egames/style.css')}}">
    @yield('head')
</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area wow fadeInDown" data-wow-delay="500ms">
        <!-- Top Header Area -->
        <div class="top-header-area">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12 d-flex align-items-center justify-content-between">
                        <!-- Logo Area -->
                        <div class="logo">
                            <a href="{{route('home')}}"><img src="{{asset('egames/img/ml-logo.jpg')}}" alt=""></a>
                        </div>

                        <!-- Search & Login Area -->
                        <div class="search-login-area d-flex align-items-center">
                            <!-- Top Search Area -->
                            <div class="top-search-area">
                                <form action="#" method="post">
                                    <input type="search" name="top-search" id="topSearch" placeholder="Search">
                                    <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                            <!-- Login Area -->
                            <div class="login-area">
                                <a href="#"><span>Login / Register</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Navbar Area -->
        <div class="egames-main-menu" id="sticker">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="egamesNav">

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="{{route('home')}}">Home</a></li>
                                    <li><a href="{{route('hero-guide')}}">Hero Guide</a></li>
                                    <li><a href="#">Pages</a>
                                        <ul class="dropdown">
                                            <li><a href="{{asset('egames/index.html')}}">Home</a></li>
                                            <li><a href="{{asset('egames/post.html')}}">Articles</a></li>
                                            <li><a href="{{asset('egames/single-post.html')}}">Single Articles</a></li>
                                            <li><a href="{{asset('egames/game-review.html')}}">Game Review</a></li>
                                            <li><a href="{{asset('egames/single-game-review.html')}}">Single Game Review</a></li>
                                            <li><a href="{{asset('egames/contact.html')}}">Contact</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{asset('egames/post.html')}}">Articles</a>
                                        <ul class="dropdown">
                                            <li><a href="{{asset('egames/post.html')}}">Articles</a></li>
                                            <li><a href="{{asset('egames/single-post.html')}}">Single Articles</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{asset('egames/single-game-review.html')}}">Reviews</a>
                                        <ul class="dropdown">
                                            <li><a href="{{asset('egames/game-review.html')}}">Game Review</a></li>
                                            <li><a href="{{asset('egames/single-game-review.html')}}">Single Game Review</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{asset('egames/contact.html')}}">Contact</a></li>
                                </ul>
                            </div>
                            <!-- Nav End -->
                        </div>

                        <!-- Top Social Info -->
                        <div class="top-social-info">
                            <a href="https://instagram.com/helcurt_mlbb28" target="_blank" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href="https://www.facebook.com/azimkodunsanak" target="_blank" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="https://m.mobilelegends.com/id" target="_blank" data-toggle="tooltip" data-placement="top" title="Official Website"><img src="{{asset('egames/img/ml-icon.png')}}" style="width: 17.5px"></i></a>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->