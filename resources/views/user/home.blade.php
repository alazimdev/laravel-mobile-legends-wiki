@include('user.layout.header')
<!-- ##### Hero Area Start ##### -->
<div class="hero-area">
    <!-- Hero Post Slides -->
    <div class="hero-post-slides owl-carousel">

        <!-- Single Slide -->
        <div class="single-slide bg-img bg-overlay"
            style="background-image: url({{asset('egames/img/background/helcurt.png')}});">
            <!-- Blog Content -->
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12 col-lg-9">
                        <div class="blog-content" data-animation="fadeInUp" data-delay="100ms">
                            <h2 data-animation="fadeInUp" data-delay="400ms">Mobile Legends?</h2>
                            <p data-animation="fadeInUp" data-delay="700ms"><strong>Mobile Legends: Bang Bang</strong>
                                adalah sebuah permainan MOBA yang dirancang untuk ponsel. Kedua tim lawan berjuang untuk
                                mencapai dan menghancurkan basis musuh sambil mempertahankan basis mereka sendiri untuk
                                mengendalikan jalan setapak, tiga "jalur" yang dikenal sebagai "top", "mid" dan
                                "bottom", yang menghubungkan basis-basis. Di masing-masing tim, ada lima pemain yang
                                masing-masing mengendalikan avatar, yang dikenal sebagai "hero", dari perangkat mereka
                                sendiri. Karakter terkontrol komputer yang lebih lemah, yang disebut "minions", bertelur
                                di basis tim dan mengikuti tiga jalur ke basis tim lawan, melawan musuh dan menara.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Single Slide -->
        <div class="single-slide bg-img bg-overlay"
            style="background-image: url({{asset('egames/img/background/aldous.png')}});">
            <!-- Blog Content -->
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12 col-lg-9">
                        <div class="blog-content" data-animation="fadeInUp" data-delay="100ms">
                            <h2 data-animation="fadeInUp" data-delay="400ms">The Power of Gaming</h2>
                            <p data-animation="fadeInUp" data-delay="700ms">Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Nunc tristique justo id elit bibendum pharetra non vitae lectus. Mauris
                                libero felis, dapibus a ultrices sed, commodo vitae odio. Sed auctor tellus quis arcu
                                tempus, egestas tincidunt.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- ##### Hero Area End ##### -->

<!-- ##### Post Details Area Start ##### -->
<section class="post-news-area section-padding-0-100">
    <div class="container">
        <div class="row justify-content-center">
            <!-- Post Details Content Area -->
            <div class="col-12 col-lg-8">
                <div class="mt-100">
                    <div class="post-details-content mb-100">
                        <!-- <div class="blog-thumbnail mb-50">
                                <img src="img/bg-img/24.jpg" alt="">
                            </div> -->
                        <div class="blog-content">
                            <h4 class="post-title">Sony’s new releases for 2018</h4>
                            <div class="post-meta mb-30">
                                <a href="#" class="post-date">July 12, 2018</a>
                                <a href="#" class="post-author">By Admin</a>
                                <a href="#" class="post-comments">2 Comments</a>
                            </div>
                            <p>“MOBA kok analog?” Pasti kalimat ini sering didengar oleh para pemain esports Mobile
                                Legends di Indonesia. Seiring dengan maraknya migrasi para game developer dari platform
                                PC ke mobile, tak heran jika permainan Mobile Legends ini menjadi alternatif yang
                                menarik bagi kamu yang ingin bermain MOBA secara praktis dan mudah.</p>
                            <p>Tidak seperti bermain di PC yang harus duduk di kursi ‘gaming’ dan menyiapkan gear
                                pendukung, Mobile Legends dapat dimainkan dimana saja, baik di kamar, maupun ketika
                                sedang bekerja atau mungkin pada saat sedang di kamar mandi. Mobile Legends telah
                                berkembang menjadi mobile game yang paling populer saat ini di Indonesia, setiap hari
                                ribuan pemain memainkan game ini.</p><br />

                            <h2>1. Apa Itu Mobile Legends?</h2>

                            <p>Mobile Legends adalah game yang dikembangkan dan dirilis oleh Moontoon developer. </p>
                            <p>Game ini dapat dimainkan di platform mobile Android dan iOS. Game MOBA satu ini sukses
                                mencuri perhatian para gamer di Indonesia sejak tahun 2016.</p>
                            <p>Permainan ini dimainkan sebanyak 10 orang yang terbagi menjadi 2 tim.</p>
                            <p>Permainan dimulai dengan setiap pemain memilih 1 hero dari daftar hero yang bisa diakses
                                oleh pemain. Hero yang tersedia adalah hero yang telah dibeli dan hero yang sedang
                                ‘dipinjamkan’ secara gratis kepada pemain oleh sistem. Dengan kata lain, tidak semua
                                hero dapat langsung dimainkan.</p>
                            <p>Lama permainan berkisar sekitar 15 menit untuk 1 ronde.</p><br />

                            <h2>2. Apa Tujuan Permainan?</h2>

                            <p>Tujuan utama permainan adalah untuk menghancurkan base lawan.</p>
                            <p>Terdapat 3 lane utama dalam map, yaitu middle, top, dan bottom lane.</p>
                            <p>Masing-masing lane dijaga oleh Turret yang akan menyerang unit musuh secara otomatis.</p>
                            <p>Setiap lane juga memiliki bangunan ‘barrack’ yang jika dihancurkan akan membuat
                                creep/minion lawan menjadi semakin kuat.</p>
                            <p>Apabila semua ‘barrack‘ hancur, maka tim lawan akan memiliki super minion yang jauh lebih
                                kuat dibanding minion biasa.</p>
                            <p>Di luar jalur lane setiap tim, juga terdapat jungle yang berisi jungle creep yang akan
                                memberikan buff pada unit yang mengalahkan creep tersebut.</p>
                            <p>Permainan berakhir ketika base salah satu tim hancur.</p><br />

                            <h2>3. Apa itu hero?</h3>

                                <p>Hero adalah karakter unik yang hanya dapat dimainkan oleh 1 player pada setiap ronde
                                    permainan.</p>
                                <p>Hero dapat dibeli dengan melakukan Top Up diamond, atau dengan menggunakan ticket dan
                                    Battle Point.</p>
                                <p>Setiap hero memiliki 4 skill, dengan 1 passive skill dan 3 active skill.</p>
                                <p>Berdasarkan jarak serangnya, hero terbagi menjadi 2 tipe, yaitu melee dan ranged.
                                    Hero melee memiliki jangkauan serang yang pendek dibandingkan dengan hero ranged.
                                </p>
                                <p>Dari karakteristiknya, hero dibagi menjadi beberapa tipe, yaitu: Marksman – Tipe hero
                                    ini memiliki daya serang yang kuat sehingga memiliki peran sebagai penyerang utama
                                    dalam permainan. Tank – Tipe hero ini memiliki base HP dan Armor yang besar sehingga
                                    dapat menerima damage yang besar dari lawan untuk melindungi tim. Mage – Tipe hero
                                    ini memiliki skill dengan damage yang besar sehingga dapat melakukan quick kill
                                    kepada hero lawan. Support – Tipe hero ini memiliki skill yang berguna untuk menjaga
                                    tim serta membantu tim ketika sedang bertempur. Assassin – Tipe hero ini umumnya
                                    memiliki skill yang dapat membunuh serta melakukan lock kepada lawan.</p><br />

                                <h2>4. Apa Itu Role?</h2>

                                <p>Mobile Legends adalah salah satu jenis permainan yang mengandalkan kekompakan tim.
                                    Oleh sebab itu, komposisi tim dengan peran yang tepat merupakan hal yang sangat
                                    penting untuk memenangkan permainan.</p>
                                <p>Role adalah pembagian tugas dalam permainan. umumnya role dibuat agar sinergi dan
                                    kekompakan tim tetap terjaga. Jenis-jenis tersebut adalah: Carry – Role yang
                                    bertugas sebagai pemberi damage utama ketika war. Umumnya merupakan hero tipe
                                    Marksman. Role carry membutuhkan intensitas farm dan levelyang tinggi untuk mencapai
                                    potensinya. Contoh hero marksman adalah Miya, Layla, dan Clint. Tank – Role yang
                                    bertugas sebagai penerima damage ketika war. Tank juga harus berada di barisan
                                    paling depan guna melindungi teman setimnya. Tank memiliki base HP dan Armor yang
                                    tinggi yang membuat mereka susah dikalahkan. Contoh hero tank adalah Balmond,
                                    Tigreal, dan Franco. Support – Role yang bertugas membantu hero carry mendapatkan
                                    farm ataupun melakukan gank pada tim lawan. Role ini tidak terlalu membutuhkan farm
                                    dan level. Contoh hero support adalah Rafaela dan Nana.</p>
                                <!-- <div class="row mt-50">
                                    <div class="col-6">
                                        <img src="{{asset('egames/img/bg-img/25.jpg')}}" alt="">
                                    </div>
                                    <div class="col-6">
                                        <img src="{{asset('egames/img/bg-img/26.jpg')}}" alt="">
                                    </div>
                                </div> -->
                        </div>
                    </div>

                    <!-- Comment Area Start -->
                    <div class="comment_area clearfix mb-70">
                        <h4 class="mb-50">Comments</h4>

                        <ol>
                            <!-- Single Comment Area -->
                            <li class="single_comment_area">
                                <!-- Comment Content -->
                                <div class="comment-content d-flex">
                                    <!-- Comment Author -->
                                    <div class="comment-author">
                                        <img src="{{asset('egames/img/bg-img/32.jpg')}}" alt="author">
                                    </div>
                                    <!-- Comment Meta -->
                                    <div class="comment-meta">
                                        <a href="#" class="post-author">William Smith</a>
                                        <a href="#" class="post-date">July 12, 2018</a>
                                        <p>Integer sed facilisis eros. In iaculis rhoncus velit in malesuada. In hac
                                            habitasse platea dictumst. Fusce erat ex, consectetur sit amet ornare
                                            suscipit, porta et erat. Donec nec nisi in nibh commodo laoreet.</p>
                                        <a href="#reply" class="reply">Reply</a>
                                    </div>
                                </div>

                                <ol class="children">
                                    <li class="single_comment_area">
                                        <!-- Comment Content -->
                                        <div class="comment-content d-flex">
                                            <!-- Comment Author -->
                                            <div class="comment-author">
                                                <img src="{{asset('egames/img/bg-img/33.jpg')}}" alt="author">
                                            </div>
                                            <!-- Comment Meta -->
                                            <div class="comment-meta">
                                                <a href="#" class="post-author">Jaku Smith</a>
                                                <a href="#" class="post-date">July 12, 2018</a>
                                                <p>Facilisis eros. In iaculis rhoncus velit in malesuada. In hac
                                                    habitasse platea dictumst. Fusce erat ex, consectetur sit amet
                                                    ornare suscipit, porta et erat. Donec nec nisi in nibh commodo
                                                    laoreet.</p>
                                                <a href="#reply" class="reply">Reply</a>
                                            </div>
                                        </div>
                                    </li>
                                </ol>
                            </li>
                        </ol>
                    </div>

                    <div class="post-a-comment-area mb-30 clearfix" id="reply">
                        <h4 class="mb-50">Leave a reply</h4>

                        <!-- Reply Form -->
                        <div class="contact-form-area">
                            <form action="#" method="post">
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <input type="text" class="form-control" id="name" placeholder="Name*">
                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <input type="email" class="form-control" id="email" placeholder="Email*">
                                    </div>
                                    <div class="col-12">
                                        <input type="text" class="form-control" id="subject" placeholder="Website">
                                    </div>
                                    <div class="col-12">
                                        <textarea name="message" class="form-control" id="message" cols="30" rows="10"
                                            placeholder="Message"></textarea>
                                    </div>
                                    <div class="col-12">
                                        <button class="btn egames-btn w-100" type="submit">Submit Comment</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Sidebar Widget -->
            <div class="col-12 col-sm-9 col-md-6 col-lg-4">
                <div class="sidebar-area mt-100">

                    <!-- Single Widget Area -->
                    <div class="single-widget-area add-widget">
                        <a href="#"><img src="{{asset('egames/img/bg-img/add.png')}}" alt=""></a>
                        <!-- Side Img -->
                        <img src="{{asset('egames/img/bg-img/side-img.png')}}" class="side-img" alt="">
                    </div>

                    <!-- Single Widget Area -->
                    <div class="single-widget-area post-widget">
                        <h4 class="widget-title">Latest Posts</h4>
                        <!-- Single Post Widget -->
                        <div class="single-post-area d-flex">
                            <div class="blog-thumbnail">
                                <img src="{{asset('egames/img/bg-img/19.jpg')}}" alt="">
                            </div>
                            <div class="blog-content">
                                <a href="#" class="post-title">New to gaming? Here are some tips</a>
                                <span>Nintendo Wii, PS4, XBox 360</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ##### Post Details Area End ##### -->

<!-- ##### Monthly Picks Area Start ##### -->
<section class="monthly-picks-area section-padding-100 bg-pattern">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="left-right-pattern"></div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Title -->
                <h2 class="section-title mb-70 wow fadeInUp" data-wow-delay="100ms">Hero Guide.</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <ul class="nav nav-tabs wow fadeInUp" data-wow-delay="300ms" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="popular-tab" data-toggle="tab" href="#popular" role="tab"
                            aria-controls="popular" aria-selected="true">All</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="latest-tab" data-toggle="tab" href="#latest" role="tab"
                            aria-controls="latest" aria-selected="false">Mage</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="editor-tab" data-toggle="tab" href="#editor" role="tab"
                            aria-controls="editor" aria-selected="false">Assassin</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="tab-content wow fadeInUp" data-wow-delay="500ms" id="myTabContent">
        <div class="tab-pane fade show active" id="popular" role="tabpanel" aria-labelledby="popular-tab">
            <!-- Popular Games Slideshow -->
            <div class="popular-games-slideshow owl-carousel">

                <!-- Single Games -->
                <div class="single-games-slide">
                    <img src="{{asset('egames/img/bg-img/50.jpg')}}" alt="">
                    <div class="slide-text">
                        <a href="#" class="game-title">Grand Theft Auto V</a>
                        <div class="meta-data">
                            <a href="#">User: 9.1/10</a>
                            <a href="#">Action</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="tab-pane fade" id="latest" role="tabpanel" aria-labelledby="latest-tab">
            <!-- Latest Games Slideshow -->
            <div class="latest-games-slideshow owl-carousel">

                <!-- Single Games -->
                <div class="single-games-slide">
                    <img src="{{asset('egames/img/bg-img/50.jpg')}}" alt="">
                    <div class="slide-text">
                        <a href="#" class="game-title">Grand Theft Auto V</a>
                        <div class="meta-data">
                            <a href="#">User: 9.1/10</a>
                            <a href="#">Action</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="tab-pane fade" id="editor" role="tabpanel" aria-labelledby="editor-tab">
            <!-- Editor Games Slideshow -->
            <div class="editor-games-slideshow owl-carousel">

                <!-- Single Games -->
                <div class="single-games-slide">
                    <img src="{{asset('egames/img/bg-img/50.jpg')}}" alt="">
                    <div class="slide-text">
                        <a href="#" class="game-title">Grand Theft Auto V</a>
                        <div class="meta-data">
                            <a href="#">User: 9.1/10</a>
                            <a href="#">Action</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@include('user.layout.footer')