<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/hero-guide', 'HeroController@index')->name('hero-guide');

Route::prefix('helcurt')->group(function () {
    Route::get('/', 'AdminController@index')->name('admin-index');
    Route::prefix('hero')->group(function () {
        Route::get('/', 'HeroController@admin_hero_index')->name('admin-hero-index');
        Route::get('/data', 'HeroController@admin_hero_data')->name('admin-hero-data');
        Route::post('/create', 'HeroController@admin_hero_create')->name('admin-hero-create');
        Route::get('/view/{id}/{heroName}', 'HeroController@admin_hero_view')->name('admin-hero-view');
        Route::get('/edit/{id}/{heroName}', 'HeroController@admin_hero_edit')->name('admin-hero-edit');
        Route::post('/update/{id}/{heroName}', 'HeroController@admin_hero_update');
        Route::get('/delete/{id}/{heroName}', 'HeroController@admin_hero_destroy');
        Route::get('/massdelete', 'HeroController@admin_hero_massdelete')->name('admin-hero-massdelete');
        Route::prefix('role')->group(function () {
            Route::get('/', 'RoleController@index')->name('admin-hero-role-index');
            Route::post('/create', 'RoleController@create')->name('admin-hero-role-create');
            Route::get('/edit/{id}/{roleName}', 'RoleController@edit')->name('admin-hero-role-edit');
            Route::post('/update/{id}', 'RoleController@update');
            Route::get('/delete/{id}/{roleName}', 'RoleController@destroy');
            Route::get('/massdelete', 'RoleController@massdelete')->name('admin-hero-role-massdelete');
        });
    });
});