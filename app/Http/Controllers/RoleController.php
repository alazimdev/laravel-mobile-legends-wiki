<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\HeroRole;
use Carbon\Carbon;
use Image;
use File;

class RoleController extends Controller
{
    public $path;
    public $dimensions;

    public function __construct()
    {
        $this->path = storage_path('app/public/image/role');
        $this->dimensions = ['100','300', '500'];
    }
    public function index()
    {
        $roles  = HeroRole::get();
        return view('admin.hero.role.index', compact('roles'));
    }
    public function create(Request $request){
        try{
            $this->validate($request, [
                'logo' => 'required|image|mimes:jpg,png,jpeg'
            ]);

            if (!File::isDirectory($this->path)) {
                File::makeDirectory($this->path);
            }

            $logo = $request->file('logo');
            $logoBaru = Carbon::now()->timestamp . '_' . uniqid() . '.' . $logo->getClientOriginalExtension();
            Image::make($logo)->save($this->path . '/' . $logoBaru);

            foreach ($this->dimensions as $row) {
                $canvas = Image::canvas($row, $row);
                $resizeImage  = Image::make($logo)->resize($row, $row, function($constraint) {
                    $constraint->aspectRatio();
                });
                if (!File::isDirectory($this->path . '/' . $row)) {
                    File::makeDirectory($this->path . '/' . $row);
                }
            
                $canvas->insert($resizeImage, 'center');
                $canvas->save($this->path . '/' . $row . '/' . $logoBaru);
            }

            $role   = new HeroRole();
            $role->role_name    = $request->input('role_name');
            $role->logo         = $logoBaru;
            $role->desc         = $request->input('desc');
            $role->save();
            return(back());
        } catch (\Exception $e){
            return $e->getMessage();
        }
    }
    public function edit($id, $roleName){
        try{
            $cek    = HeroRole::find($id);
            $imglink= Storage::url('app/public/image/role/'.$cek->logo);
            if($cek->role_name == $roleName){
                return view('admin.hero.role.edit', compact('cek','imglink'));
            } else {
                echo 'ini gak cocok bro';
            }
        } catch (\Exception $e){
            return view("404");
        }
    }
    public function update(Request $request, $id){
        try {
            $role = HeroRole::find($id);
            if($request->file('logo') != null){
                $logo = $request->file('logo');
                $this->validate($request, [
                    'logo' => 'required|image|mimes:jpg,png,jpeg'
                ]);

                unlink($this->path.'/'.$role->logo);
                foreach($this->dimensions as $row){
                    unlink($this->path.'/'.$row.'/'.$role->logo);
                }
                $logoBaru = Carbon::now()->timestamp . '_' . uniqid() . '.' . $logo->getClientOriginalExtension();
                Image::make($logo)->save($this->path . '/' . $logoBaru);

                foreach ($this->dimensions as $row) {
                    $canvas = Image::canvas($row, $row);
                    $resizeImage  = Image::make($logo)->resize($row, $row, function($constraint) {
                        $constraint->aspectRatio();
                    });
                    $canvas->insert($resizeImage, 'center');
                    $canvas->save($this->path . '/' . $row . '/' . $logoBaru);
                }
                $role->logo = $logoBaru;
            }
            
            $role->role_name    = $request->input('role_name');
            $role->desc         = $request->input('desc');
            $role->save();
            return redirect()->route('admin-hero-role-index');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
    public function destroy($id, $roleName){
        try{
            $role = HeroRole::find($id);
            if($role->role_name == $roleName){
                unlink($this->path.'/'.$role->logo);
                foreach($this->dimensions as $row){
                    unlink($this->path.'/'.$row.'/'.$role->logo);
                }
            }
            $role->delete();
            return(back());
        } catch (\Exception $e) {
            return view("404");
        }

    }
    public function massdelete(Request $req)
    {
        try{
            $role_id_array = $req->input('id');
            foreach($role_id_array as $data){
                $file   = HeroRole::where('id', $data)->get();
                foreach($file as $logo){
                    unlink($this->path.'/'.$logo->logo);
                    foreach($this->dimensions as $row){
                        unlink($this->path.'/'.$row.'/'.$logo->logo);
                    }
                }
            }
            $role = HeroRole::whereIn('id', $role_id_array);
            $role->delete();
        } catch (\Exception $e) {
            return view('404');
        }
    }
}
